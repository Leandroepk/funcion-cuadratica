package com.leandr00o.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText etA;
    private EditText etB;
    private EditText etC;

    private TextView tvr1;
    private TextView tvr2;
    private TextView tves;
    private TextView tvv;
    private TextView tvoo;

    private TextView tvdom;
    private TextView tvimg;
    private TextView tvc0;
    private TextView tvcc;
    private TextView tvcd;
    private TextView tvcp;
    private TextView tvcn;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etA = (EditText)findViewById(R.id.A);
        etB = (EditText)findViewById(R.id.B);
        etC = (EditText)findViewById(R.id.C);

        tvr1 = (TextView)findViewById(R.id.r1);
        tvr2 = (TextView)findViewById(R.id.r2);
        tves = (TextView)findViewById(R.id.es);
        tvv = (TextView)findViewById(R.id.v);
        tvoo = (TextView)findViewById(R.id.oo);

        tvdom = (TextView)findViewById(R.id.dom);
        tvimg = (TextView)findViewById(R.id.img);
        tvc0 = (TextView)findViewById(R.id.c0);
        tvcc = (TextView)findViewById(R.id.cc);
        tvcd = (TextView)findViewById(R.id.cd);
        tvcp = (TextView)findViewById(R.id.cp);
        tvcn = (TextView)findViewById(R.id.cn);


    }

    public void main(View view) {
        float A;
        if(etA.getText().length()<1){A = 1;}
        else{A = Integer.parseInt(etA.getText().toString());}

        float B;
        if(etB.getText().length()<1){B = 1;}
        else{B = Integer.parseInt(etB.getText().toString());}

        float C;
        if(etC.getText().length()<1){C = 1;}
        else{C = Integer.parseInt(etC.getText().toString());}

        //Toast.makeText(getApplicationContext(),raizN+"",Toast.LENGTH_SHORT).show();
        if(A!=0){
            boolean ban = false;
            double raizN = (B*B)+(-4*A*C);
            String raiz = "";
            String raiz1 = "";
            String raiz2 = "";
            if(raizN>=0){
                if((Math.sqrt(raizN)*1000) % 10 > 0){
                    ban = true;
                    raiz = "√"+Math.round(raizN);
                }
                else {
                    raiz = Math.sqrt(raizN)+"";
                    raizN = Math.sqrt(raizN);
                }
                if(!ban){
                    if((((-B-raizN)/(2*A))*1000) % 10 > 0){
                        if(A>0){
                            raiz1 = (-B-raizN) + "/" + Math.round(2 * A);
                        }
                        else {
                            raiz2 = (-B-raizN) + "/" + Math.round(2 * A);
                        }
                    }
                    else {
                        if(A>0){
                            raiz1 = (-B-raizN)/(2*A)+"";
                        }
                        else {
                            raiz2 = (-B-raizN)/(2*A)+"";
                        }
                    }

                    if( ((-B+raizN)/(2*A)*1000) % 10 > 0){
                        if(A>0){
                            raiz2 = (-B+raizN) + "/" + (2*A);
                        }
                        else {
                            raiz1 = (-B+raizN) + "/" + (2*A);
                        }
                    }
                    else {
                        if(A>0){
                            raiz2 = (-B+raizN)/(2*A)+"";
                        }
                        else {
                            raiz1 = (-B+raizN)/(2*A)+"";
                        }
                    }
                }
                else{
                    if(A>0){
                        raiz1 = "("+(-B)+"-"+raiz+")/"+(2*A);
                        raiz2 = "("+(-B)+"+"+raiz+")/"+(2*A);
                    }
                    else {
                        raiz1 = "("+(-B)+"+"+raiz+")/"+(2*A);
                        raiz2 = "("+(-B)+"-"+raiz+")/"+(2*A);
                    }
                }

                tvr1.setText(getString(R.string.r1)+" "+raiz1);
                tvr2.setText(getString(R.string.r2)+" "+raiz2);
            }
            else {
                tvr1.setText(getString(R.string.r1)+" no tiene");
                tvr2.setText(getString(R.string.r2)+" no tiene");
            }

            float es;
            if(B==0){es=0;}
            else{es = (-B) / (2*A);}
            tves.setText(getString(R.string.es)+" "+es);
            float vy = A*es*es+B*es+C;
            tvv.setText(getString(R.string.v)+" ( "+es+" ; "+vy+" )");

            tvoo.setText(getString(R.string.oo)+" "+C);

            tvdom.setText(getString(R.string.dom)+" R");

            if(A>0){
                tvimg.setText(getString(R.string.img)+" [ "+vy+" ; +∞ )");
            }
            if(A<0){
                tvimg.setText(getString(R.string.img)+" ( -∞ ; "+vy+" ]");
            }

            if(raiz1 != "" && raiz2 != ""){
                tvc0.setText(getString(R.string.c0)+" { "+raiz1+" ; "+raiz2+" }");
                if(A>0){
                    tvcp.setText(getString(R.string.cp)+" ( -∞ ; "+raiz1+" )U( "+raiz2+" ; +∞ )");
                    tvcn.setText(getString(R.string.cn)+" ( "+raiz1+" ; "+raiz2+" )");
                }
                if(A<0){
                    tvcp.setText(getString(R.string.cp)+" ( "+raiz1+" ; "+raiz2+" )");
                    tvcn.setText(getString(R.string.cn)+" ( -∞ ; "+raiz1+" )U( "+raiz2+" ; +∞ )");
                }
            }
            else{
                tvc0.setText(getString(R.string.c0)+" no tiene");
                if(A>0){
                    tvcp.setText(getString(R.string.cp)+" R");
                    tvcn.setText(getString(R.string.cn)+" no tiene");
                }
                if(A<0){
                    tvcp.setText(getString(R.string.cp)+" no tiene");
                    tvcn.setText(getString(R.string.cn)+" R");
                }
            }
            if(A>0){
                tvcc.setText(getString(R.string.cc)+" ( "+es+" ; +∞ )");
                tvcd.setText(getString(R.string.cd)+" ( -∞ ; "+es+" )");
            }
            if(A<0){
                tvcc.setText(getString(R.string.cc)+" ( -∞ ; "+es+" )");
                tvcd.setText(getString(R.string.cd)+" ( "+es+" ; +∞ )");
            }
        }
    }
}
